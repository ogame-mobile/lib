import { HttpResponse, HttpService } from '../http-service'
import { LoginSession, Account } from '../types/login'

export class LoginClient {
  public constructor (private httpService: HttpService, private userAgent: string = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36') {
  }

  public login (username: string, password: string): Promise<HttpResponse<LoginSession>> {
    const headers = {
      'Content-Type': 'application/json',
      'user-agent': this.userAgent
    }
    return this.httpService.post('https://gameforge.com/api/v1/auth/thin/sessions', {
      identity: username,
      password: password,
      gfLang: 'en',
      locale: 'en_EN',
      platformGameId: '1dfd8e7e-6e1a-4eb1-8c64-03c3b62efd2f',
      gameEnvironmentId: '0a31d605-ffaf-43e7-aa02-d06df7116fc8',
      autoGameAccountCreation: false
    }, headers)
  }

  public myAccounts (token: string): Promise<HttpResponse<Account[]>> {
    const headers = {
      Authorization: `Bearer ${token}`,
      'user-agent': this.userAgent
    }
    return this.httpService.get('https://lobby.ogame.gameforge.com/api/users/me/accounts', undefined, headers)
  }
}
