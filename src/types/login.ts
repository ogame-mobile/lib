export interface LoginSession {
  token: string
  isPlatformLogin: boolean
  isGameAccountMigrated: boolean
  platformUserId: string
  isGameAccountCreated: boolean
  hasUnmigratedGameAccounts: boolean
}

export interface Account {
  server: {
    language: string
    number: number
  }
  id: number
  gameAccountId: number
  name: string
  lastPlayed: string
  lastLogin: string
  blocked: boolean
  bannedUntil: string | null
  bannedReason: string | null
  detail: {
    type: string
    title: string
    value: string
  }[]
  sitting: {
    shared: boolean
    endTime: string | null
    cooldownTime: number | null
  }
  trading: {
    trading: boolean
    cooldownTime: number | null
  }
}
