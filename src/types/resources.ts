export interface Resources {
  metal: number
  crystal: number
  deuterium: number
  darkmatter: number
  energy: number
}
