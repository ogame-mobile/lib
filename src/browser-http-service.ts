import { HttpResponse, HttpService } from './http-service'

/* global RequestInit */

export class BrowserHttpService implements HttpService {
  public get<T> (url: string, body?: any, headers = {}): Promise<HttpResponse<T>> {
    return this.fetchBodyParser(url, {
      body: body ? JSON.stringify(body) : undefined,
      headers: new Headers(headers),
      method: 'GET'
    })
  }

  public post<T> (url: string, body?: any, headers = {}): Promise<HttpResponse<T>> {
    return this.fetchBodyParser(url, {
      body: body ? JSON.stringify(body) : undefined,
      headers: new Headers(headers),
      method: 'POST'
    })
  }

  public put (url: string): Promise<any> {
    throw new Error('Method not implemented.')
  }

  public delete (url: string): Promise<any> {
    throw new Error('Method not implemented.')
  }

  private async fetchBodyParser<T> (url: string, init?: RequestInit): Promise<HttpResponse<T>> {
    const response = await fetch(url, init)
    let body: T
    if (response.headers.get('Content-Type')?.includes('application/json')) {
      body = await response.json()
    } else {
      body = await response.text() as unknown as T
    }
    return new HttpResponse(body, response.status)
  }
}
