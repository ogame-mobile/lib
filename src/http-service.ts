export class HttpResponse<T> {
  public constructor (private readonly _body: T, private readonly _statusCode: number) {
  }

  public get body (): T {
    return this._body
  }

  public get statusCode (): number {
    return this._statusCode
  }

  public isSuccessfull (): boolean {
    return this._statusCode < 400
  }
}

export interface HttpService {
  get<T> (url: string, body?: any, headers?: {[headerName: string]: string}): Promise<HttpResponse<T>>
  post<T>(url: string, body?: any, headers?: {[headerName: string]: string}): Promise<HttpResponse<T>>
  put(url: string): Promise<any>
  delete(url: string): Promise<any>
}
