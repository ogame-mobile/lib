import { HttpService } from './http-service'

export interface OgameClientOptions {
  serverNumber: number
  serverLanguage: string
  cookie: string
}

export class OgameClient {
  public constructor (private options: OgameClientOptions, private httpService: HttpService) {
  }
}
